package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/mergeapi"
)

var (
	appName = "xdc"
	mapi    = &mergeapi.MergeAPI{AppName: appName}
	Version = "undefined"
	nocolor = false
)

func init() {
	root.PersistentFlags().BoolVarP(
		&nocolor, "nocolor", "n", false, "disable color in output",
	)
}

var root = &cobra.Command{
	Use:   appName,
	Short: "eXperiment Development Container tool",
}

func main() {

	cobra.EnablePrefixMatching = true

	version := &cobra.Command{
		Use:   "version",
		Short: "Show the mergexp version",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			fmt.Println(Version)
		},
	}
	root.AddCommand(version)

	show := &cobra.Command{
		Use:   "show",
		Short: "Show xdc related things",
	}
	root.AddCommand(show)

	autocomplete := &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			root.GenBashCompletion(os.Stdout)
		},
	}
	root.AddCommand(autocomplete)

	authCmds(root, show)
	ansibleCmds(root, show)
	generateCmds(root)
	tunnelCmds(root, show)
	powerCmds(root)

	root.Execute()
}
