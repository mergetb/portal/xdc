VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X main.Version=$(VERSION)"

prefix ?= /usr/local

.PHONY: all
all: build/xdc

build:
	$(QUIET) mkdir -p build

build/xdc: main.go *.go | build
	$(call go-build)

.PHONY: clean
clean:
	rm -rf build

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@printf "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)\n"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=${LDFLAGS} -o $@
endef

build/xdc.bash_autocomplete: build/xdc
	build/xdc autocomplete > $@

# Note you problably do not want to `make install` on a non-XDC.
.PHONY: install
install: build/xdc build/xdc.bash_autocomplete
	install -D build/xdc $(DESTDIR)$(prefix)/bin/xdc
	install -D build/xdc.bash_autocomplete $(DESTDIR)$(prefix)/share/bash-completion/completions/xdc

