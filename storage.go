package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

var xdcSysDir string = "/etc/xdc"

func writeFile(data, filename string) error {

	err := os.MkdirAll(xdcSysDir, 0700)
	if err != nil {
		return fmt.Errorf("failed to mk %s dir: %w", xdcSysDir, err)
	}

	err = ioutil.WriteFile(
		fmt.Sprintf("%s/%s", xdcSysDir, filename),
		[]byte(data),
		0664,
	)
	if err != nil {
		return fmt.Errorf("failed to write data to %s/%s: %w", xdcSysDir, filename, err)
	}

	return nil
}

func readFile(filename string) (string, error) {

	path := fmt.Sprintf("%s/%s", xdcSysDir, filename)
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return "", fmt.Errorf("missing file %s: %w", path, err)
		}
		return "", err
	}

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return "", fmt.Errorf("failed to read file %s: %w", path, err)
	}

	return string(buf), nil
}

func deleteFile(filename string) error {

	path := fmt.Sprintf("%s/%s", xdcSysDir, filename)
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return nil
		}
	}

	err := os.Remove(path)
	if err != nil {
		doError(err)
	}

	return nil
}
