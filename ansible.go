package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	rz "gitlab.com/mergetb/engine/api/realize"
	xir "gitlab.com/mergetb/xir/lang/go/v0.2"
)

func ansibleCmds(root, show *cobra.Command) {

	ansible := &cobra.Command{
		Use:   "ansible [cmd]",
		Short: "Execute ansible related commands",
	}

	ansid := &cobra.Command{
		Use:   "inventory [<pid> <eid> <rid>]",
		Short: "Generate an ansible inventory file for the given materialization or the attached one if none is given",
		Args:  cobra.MaximumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			exitIfUnattached()
			if len(args) != 3 {
				dumpInv("", "", "")
			} else {
				dumpInv(args[0], args[1], args[2])
			}
		},
	}
	ansible.AddCommand(ansid)

	ping := &cobra.Command{
		Use:   "ping",
		Short: "Call ansible ping on all attached experiment nodes",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			exitIfUnattached()
			ansiblePing()
		},
	}
	ansible.AddCommand(ping)

	root.AddCommand(ansible)
}

// getModel retrieves the model for the give pid/eid/rid realization. If the args are "", then
// getModel will look for an attached materialization and use that.
func getModel(p, e, r string) *xir.Net {

	pid, eid, rid := p, e, r

	if pid == "" {
		var err error
		pid, eid, rid, err = readTunnelInfo()
		if err != nil {
			fmt.Println("xdc may not be attached: no materialization information.")
			os.Exit(1)
		}
	}

	rlzresp, err := mapi.Realization(pid, eid, rid)
	if err != nil {
		doError(err)
	}

	log.Debugf("raw realization: %+v", rlzresp)

	// Do the marshal/unmarshal dance to rebuild the datatype.
	bytes, _ := json.Marshal(rlzresp)
	rlz := &rz.Realization{}
	json.Unmarshal(bytes, rlz)

	// Get the model for the realization.
	xpSrcResp, err := mapi.PullExperimentModel(pid, eid, rlz.Xhash)
	if err != nil {
		log.Fatal(err)
	}

	model, err := xir.FromString(xpSrcResp.Xir)
	if err != nil {
		log.Fatal(err)
	}

	return model
}

func dumpInv(pid, eid, rid string) {

	dumpAnsibleInventory(getModel(pid, eid, rid))
}

func dumpAnsibleInventory(model *xir.Net) {

	all := []string{}
	grps := make(map[string][]string)

	for _, n := range model.AllNodes() {

		name := n.Label()
		all = append(all, name)

		// look for groups.
		if grp, ok := n.Props.GetString("group"); ok {
			grps[grp] = append(grps[grp], name)
		}
	}

	sort.Strings(all)

	fmt.Printf("#\n# Ansible Inventory File - auto-generated\n#\n")

	fmt.Println("[all]")
	for _, n := range all {
		fmt.Println(n)
	}
	for grp, names := range grps {
		fmt.Println()
		fmt.Printf("[%s]\n", grp)

		for _, n := range names {
			fmt.Println(n)
		}
	}

}

func ansiblePing() {

	model := getModel("", "", "")

	names := []string{}
	for _, n := range model.AllNodes() {
		names = append(names, n.Label())
	}
	nodes := strings.Join(names, ",")

	cmd := exec.Command("ansible", "-i", nodes, "all", "-m", "ping")

	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Printf("ansible error. output: \n%s\n", out)
		doError(err)
	}

	fmt.Println(string(out))
}
