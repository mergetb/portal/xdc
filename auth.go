package main

import (
	"fmt"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/mergeapi"
	"golang.org/x/crypto/ssh/terminal"
)

func authCmds(root, show *cobra.Command) {

	var (
		passwd string
		api    string
		cert   string
	)

	long := `
Login to the Merge Portal. 

There are currently two options for user authentication when logging in: Oauth (via
auth0.com) and the internal identity server in the Merge portal itself (via 
user.mergetb.net). Depending on which method you used to create your account,
you will have to choose either "auth0" or "internal".

Auth0 is assumed unless you specfically choose the internal identity server 
via "-a internal".
`
	var authmode = ""
	login := &cobra.Command{
		Use:   "login <username>",
		Short: "Login to the Merge portal",
		Long:  long,
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			login(args[0], passwd, api, cert, authmode)
		},
	}
	login.Flags().StringVarP(&passwd, "passwd", "", "", "user password")
	login.Flags().StringVarP(&api, "api", "", "api.mergetb.net", "merge API endpoint")
	login.Flags().StringVarP(&cert, "certificate", "c", "", "path to API certificate")
	login.Flags().StringVarP(&authmode, "auth", "a", "",
		fmt.Sprintf("Authenication mode: must be either \"auth0\" or \"internal\""))
	root.AddCommand(login)

	logout := &cobra.Command{
		Use:   "logout",
		Short: "Logout of the merge api",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			logout()
		},
	}
	root.AddCommand(logout)

	showLoginCmd := &cobra.Command{
		Use:   "login",
		Short: "Show information about the current login",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			showLogin()
		},
	}
	show.AddCommand(showLoginCmd)
}

func login(user, passwd, api, cert, authmode string) {

	// Note this is the global MergeAPI instance.
	mapi = &mergeapi.MergeAPI{
		AppName:  appName,
		APIUser:  user,
		URL:      api,
		Certpath: cert,
	}

	if authmode != "" {
		if authmode == "internal" {
			mapi.SetIdentityServer("login.mergetb.net")
		}
		// else oauth is default
	}

	if passwd == "" {

		pw, err := readPasswd()
		if err != nil {
			log.Fatalf("Reading password: %s", err)
		}

		passwd = pw
	}

	err := mapi.Login(passwd)
	if err != nil {
		log.Fatalf("Error logging in: %s", err)
	}
}

func logout() {

	err := mapi.Logout()
	if err != nil {
		log.Fatal(err)
	}
}

func showLogin() {

	if mapi.LoggedIn() {
		u, err := mapi.MergeUser()
		if err != nil {
			doError(err)
		}
		fmt.Printf("User: %s\n", u)

		a, err := mapi.API()
		if err != nil {
			doError(err)
		}
		fmt.Printf("API: %s\n", a)

		c, err := mapi.Certificate()
		if err == nil {
			fmt.Printf("Certificate:\n%s\n", c)
		}

	} else {
		fmt.Println("Not logged in.")
	}
}

func readPasswd() (string, error) {

	fmt.Printf("password: ")

	pw, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return "", err
	}
	fmt.Println()

	return strings.TrimSpace(string(pw)), nil
}
