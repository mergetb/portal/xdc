#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/xdc*.build*
rm -f build/xdc*.change
rm -f build/xdc*.deb

if [[ ! -z $MAPIVER ]]; then
    debuild -e V=1 \
        -e prefix=/usr \
        $DEBUILD_ARGS \
        --dpkg-buildpackage-hook="go get gitlab.com/mergetb/mergeapi@${MAPIVER}" \
        -i -us -uc -b
else
    debuild -e V=1 \
        -e prefix=/usr \
        $DEBUILD_ARGS \
        -i -us -uc -b
fi

mv ../xdc*.build* build/
mv ../xdc*.changes build/
mv ../xdc*.deb build/
