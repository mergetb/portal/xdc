package main

import (
	"errors"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/engine/pkg/merror"
	"gitlab.com/mergetb/mergeapi"
)

var (
	NotAttached = errors.New("Not attached")
)

func doError(err error) {

	if errors.Is(err, mergeapi.ErrNotLoggedin) {
		fmt.Println("Please login before executing commands.")
		os.Exit(1)
	}

	if errors.Is(err, NotAttached) {
		fmt.Println(err)
		os.Exit(1)
	}

	var merr *merror.MergeError
	if errors.As(err, &merr) {
		log.Fatalf(merr.String())
	}

	log.Fatalf("Error: %+v", err)
}
